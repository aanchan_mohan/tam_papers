This repo contains a list of papers that might be useful for R&D. There are 5 categories and hence 5 folders:

1. Acoustic Modelling - Includes acoustic modelling and feature processing
2. Language Modelling 
3. Parsing - Natural Language Understanding of ASR outputs
4. Search - ASR Decoding and search
5. ASR - General Automated Speech Recognition Reading

To do:
Created a curated index linking file names to the actual pdf's...perhaps using something like JabRef...

For comments or feedback e-mail : aanchan@gmail.com

To get this, assuming you have git installed, just do:
git clone https://aanchan_mohan@bitbucket.org/aanchan_mohan/tam_papers.git
or 
git pull https://aanchan_mohan@bitbucket.org/aanchan_mohan/tam_papers.git